export const Language = {
    esp: {
        name: "esp",
        login: {
            msg_wellcome:"Por favor inicie sesion",
            button_login_txt:"Ingresar",
            input_email_txt: "Correo Electroníco",
            input_password_txt: "Contraseña",
        },
        dashboard: {
            title_texting:"Probando reorganizacion drag and drop con Dragula",
            input_name_text:"Nombre",
            input_description_text: "Descripcón",
            button_add_txt:"Agregar",
        },
        navbar: {
            name_app: "Pueba DrAngular",
            logout_button: "Cerrar sesion",
        }
    },
    eng: {
        name: "eng",
        login: {
            msg_wellcome:"Please sign in",
            button_login_txt:"Sign in",
            input_email_txt: "Email address",
            input_password_txt: "password",
        },
        dashboard: {
            title_texting:"Testing drag and drop reorganization with Dragula",
            input_name_text:"Name",
            input_description_text: "Description",
            button_add_txt:"Add",
        },
        navbar: {
            name_app: "Test DrAngular",
            logout_button: "Logout",
        }
    }
}