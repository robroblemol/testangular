import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from "rxjs";

import { AuthService } from '../services/auth-service'
import { User } from "../models/user.model";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email = '';
  password = '';
  userAuth : Observable<User>;

  constructor(private authSvc: AuthService, private router : Router) { }

  ngOnInit(): void {
  }

  onKeyEmail(email:string){
    this.email=email;
    // console.log(this.email);
    
  }

  onKeyPassword(password:string){
    this.password=password;
    // console.log(this.password);
    
  }

  login(){
    console.log(this.email);
    console.log(this.password);
    if(this.email != '' && this.password != ''){
      this.userAuth = this.authSvc
        .login({email:this.email,password:this.password})
      this.userAuth.subscribe((res)=>{
        console.log(res[0].email);
        if(res[0].email == this.email && res[0].password === this.password){
            this.email='';
            this.password='';
            localStorage.setItem('user',JSON.stringify(res));
            this.router.navigate(['dashboard']);
          }
        }
      )
    }  
  }
}
