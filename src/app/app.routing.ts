import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from "./shared/guard/auth.guard";
import { CompressImageComponent } from "./compress-image/compress-image.component";
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
    {
      path: '', 
      redirectTo: 'login',
      pathMatch: 'full',
    } ,
    {
      path: 'dashboard', 
      component: DashboardComponent, //Only registed users
      canActivate: [ AuthGuard ],
    },
    {
      path: 'compressimage',
      component: CompressImageComponent,
    },
    {
      path: 'login',
      component: LoginComponent,
    }
  ];

  @NgModule({
    imports:[RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }
