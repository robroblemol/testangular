import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { DragulaModule } from 'ng2-dragula';
import { NgxImageCompressService } from "ngx-image-compress";

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { AuthService } from './services/auth-service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ItemService } from "./services/item.service";
import { NavbarComponent } from './components/navbar/navbar.component'
import { LoginComponent } from './login/login.component';
import { LanguageService } from "./services/language.service";
import { CompressImageComponent } from './compress-image/compress-image.component';



@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    NavbarComponent,
    CompressImageComponent,
  ],
  imports: [
    BrowserModule,
    DragulaModule.forRoot(),
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [
    AuthService,
    NgxImageCompressService,
    LanguageService,
    ItemService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
