import { Component, OnInit, Input } from '@angular/core';

import { DragulaService } from 'ng2-dragula'
import { Observable } from "rxjs";

import { LanguageService } from "../services/language.service";
import { Item } from "../models/item.model";
import { ItemService } from "../services/item.service";

// export interface Item  {
//   title: '',
//   description: '',
// }

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})


export class DashboardComponent implements OnInit {
  
  
  
  newItem: Item;
  items = [];
  getItems : Observable<Item[]>;
  title = "";
  description = "";



  
  
  constructor( 
    private dragualservice: DragulaService,
    public language:LanguageService,
    private itemSvc:ItemService ) { 
    dragualservice.createGroup('ITEMS',{
      moves: (el, container, handle)=>{
        return handle.classList.contains('handle');    
      }
    })
  }

  ngOnInit(): void {

    this.getItems = this.itemSvc
      .getItems();
    this.getItems.subscribe((res) =>{
      this.items = res;
      console.log(res);
      
    });
  }

  onkeyTitle(title: string){
    this.title = title;
  }
  onKeyDescription(description: string){
    this.description = description;
    // console.log(this.description);
    
  }

  addItem(){
    if(this.title != '' && this.description != ''){
      const index = this.items.length + 1;
      this.itemSvc.setItem({id: `${index}`,title: this.title, description: this.description})
        .subscribe(res => {
          this.items.push(res);
          this.description='';
          this.title='';

        })

    }
  }

}
