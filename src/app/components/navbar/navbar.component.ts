import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LanguageService } from "../../services/language.service";


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  texts:{name:string
    login: {
      msg_wellcome:string,
      button_login_txt:string,
      input_email_txt: string,
      input_password_txt: string,
  },
  dashboard: {
      title_texting:string,
      input_name_text:string,
      input_description_text: string,
      button_add_txt:string,
  },
  navbar: {
      name_app: string,
      logout_button: string,
  }
  }
  seletedLanguage:boolean=false;

  constructor(private router : Router, public language:LanguageService) { }

   private setLanguage(l:string){
    this.language.setLanguage(l);
    this.texts = this.language.getLanguage();
    (this.texts.name == "eng")? this.seletedLanguage = true :
     this.seletedLanguage = false
     console.log(this.language.getLanguage());
     
   }
   
   ngOnInit(): void {
    this.setLanguage("esp");
    console.log(this.texts);
  }


  
  setSpanish (){
    this.setLanguage("esp");
  }
  setEnglish (){
   this.setLanguage("eng");
 }

  // spellIn = this.language.getLanguage();
  
  logout(){
    localStorage.removeItem('user')
    this.router.navigate(['login']);
  }

}
