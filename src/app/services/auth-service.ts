import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';

import { User } from '../models/user.model'


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  URL_API=``


  constructor(private http:HttpClient) { }

  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null) ? true : false;
  }
  
  login(user: { email: string, password:string}) : Observable<User> {
    this.URL_API=`/users?email=${user.email}&password=${user.password}`
    return this.http.get<User>(this.URL_API)
      .pipe(map ((data:any):User => data))
  }
}
