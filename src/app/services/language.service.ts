import { Injectable } from '@angular/core';
import { Language } from "../../assets/constants/language";

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  constructor() { }

  spell:string="esp";

  setLanguage(t:string) {

      this.spell = t;
    
  }

  getLanguage () {
    if(this.spell == "esp"){
      return Language.esp;
    }else if(this.spell == "eng"){
      return Language.eng;
    }
  }

}
