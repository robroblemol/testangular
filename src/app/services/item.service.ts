import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'


import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
// import { catchError } from 'rxjs/operators';

import { Item } from "../models/item.model";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  URL_API=``;

  constructor(private http:HttpClient) { }

  getItems() : Observable<Item[]> {
    this.URL_API=`/items`;

    return this.http.get<Item[]>(this.URL_API=`/items`)
      .pipe(map((data:any):Item[] => data))
  }
  setItem(item:Item) : Observable<Item> {
    this.URL_API=`/items`;
    return this.http.post<Item>(this.URL_API, item, httpOptions)
    .pipe(
      map((data:any):Item => data)
    );
  }
}
